import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';

import { HomeModule } from './home/home.module';
import { LoginComponent } from './login/login.component';

import { HttpClientModule} from '@angular/common/http';
import { CategoryComponent } from './category/category.component';
import { CategoryItemComponent } from './category/category-item/category-item.component';
import { CourseListComponent } from './category/course-list/course-list.component';
import { CourseDetailComponent } from './category/course-list/course-item/course-detail/course-detail.component';
import { CourseItemComponent } from './category/course-list/course-item/course-item.component';
import { ChapterComponent } from './category/course-list/course-item/course-detail/chapter/chapter.component';
import { LessonComponent } from './category/course-list/course-item/course-detail/chapter/lesson/lesson.component';
import { LessonEditComponent } from './category/course-list/course-item/course-detail/chapter/lesson/lesson-edit/lesson-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LandingComponent,
    ProfileComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    CategoryComponent,
    CategoryItemComponent,
    CourseListComponent,
    CourseDetailComponent,
    CourseItemComponent,
    ChapterComponent,
    LessonComponent,
    LessonEditComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

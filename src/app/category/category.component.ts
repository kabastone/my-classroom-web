import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ClassroomService } from '../shared/classroom.service';
import { Category } from '../shared/model/category.model';
import { Course } from '../shared/model/course.model';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  catname:string;
  category = new Category();
  courses:Course[];
  constructor(
    private route:ActivatedRoute, 
    private classRoomService:ClassroomService) { }

  ngOnInit(): void {
    this.route.params
    .subscribe((params:Params) => {
      this.catname = params['name'];
      this.classRoomService.fetCategoryByName(this.catname).subscribe(
        data => {
          this.category = data;
          this.getCourses(this.category)
        }
      )
    });
     /* this.classRoomService.fetchCourseCategory(this.category._links.courses.href)
    .subscribe(
      data => {
        this.courses = data;
        console.log(this.courses);
      }
    ) */
  }
   getCourses(category:Category) {
    this.classRoomService.fetchCourseCategory(category._links.courses.href)
    .subscribe(
      data => {
       this.courses = data._embedded.courses;
      }
    )
   }
}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Lesson } from 'src/app/shared/model/lesson.model';
import { ClassroomService } from 'src/app/shared/classroom.service';

@Component({
  selector: 'app-lesson-edit',
  templateUrl: './lesson-edit.component.html',
  styleUrls: ['./lesson-edit.component.css']
})
export class LessonEditComponent implements OnInit {
  file: any;

  constructor(private classroomService:ClassroomService) { }

  ngOnInit(): void {
  }

  onFileSelect(event){
    if(event.target.files.length > 0){
       this.file = event.target.files[0];
    }
    console.log(this.file)
  }
  onAddLesson(form: NgForm){
    const value = form.value;
    const lesson = new Lesson(value.title, this.file, value.duree, value.source)
    this.classroomService.saveLesson(lesson).subscribe(
      res => console.log(res),
      err => console.log(err)
    );
         
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { Course } from 'src/app/shared/model/course.model';
import { ActivatedRoute, Params } from '@angular/router';
import { ClassroomService } from 'src/app/shared/classroom.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {
  courseUrl: string;
  courseDetails: Course = new Course();
  constructor(private route:ActivatedRoute, private classroomService: ClassroomService) { }

  ngOnInit(): void {
     this.route.queryParams.subscribe(
       (queryParams:Params) => {
         this.courseUrl = queryParams['courseDetails'] != null? queryParams['courseDetails']: 'null'
         this.classroomService.fetchData(this.courseUrl).subscribe(
           (data: Course) => {
              this.courseDetails = data;
              
           }
         )
       }
     )
  }

}

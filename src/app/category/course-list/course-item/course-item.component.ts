import { Component, OnInit, Input } from '@angular/core';
import { Course } from 'src/app/shared/model/course.model';
import { ClassroomService } from 'src/app/shared/classroom.service';

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css']
})
export class CourseItemComponent implements OnInit {
  @Input() course:Course;
  author={};
  imgPath = {};

  constructor(public classroomservice:ClassroomService) { }
  
  ngOnInit(): void {
    this.classroomservice.fetCourseAuthor(this.course._links.author.href).subscribe(
      data => this.author = data
    )
    this.classroomservice.getcourseImg()
    .subscribe(
      response => {
        this.imgPath = response.hasOwnProperty("urls")? response['urls']: '';
        
      } 
    );
  }
  getAuthor() {

  }

}

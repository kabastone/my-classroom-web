import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Category } from './model/category.model';
import { Course } from './model/course.model';
import { Lesson } from './model/lesson.model';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {
  
  
   SERVER_URL = 'http://localhost:8080';

  constructor(public httpClient:HttpClient) { }

  saveLesson(lesson:Lesson) {
    const formData = new FormData();
    formData.append('title', lesson.title)
    formData.append('videoUrl', lesson.videoUrl)
    formData.append('duration', lesson.duration.toString())
    formData.append('codeSourceUrl', lesson.codeSourceUrl)
    
   return this.httpClient.post(this.SERVER_URL + '/lessons/new', formData,
    )
  }

  fetchALLCategories() {
    return this.httpClient.get<{_embedded:{categories:Category[]}, _links}>(this.SERVER_URL + '/categories')
  }

  fetCategoryByName(name:string) {
    return this.httpClient
    .get<Category>(this.SERVER_URL + '/categories/search/categoryNameStartWith', 
    {params: new HttpParams().set('name', name)})
  }

  fetchCourseCategory(url:string) {
    return this.httpClient.get<{_embedded:{courses:Course[]}, 
    _links:{self: {href:string}}}>(url);
  }
  fetCourseAuthor(href:string) {
    return this.httpClient.get(href);;
  }
  getcourseImg() {
    return this.httpClient.get("http://api.unsplash.com/photos/random", 
    {params: new HttpParams().set('client_id', 'iXWbGj2-W8GNusjIlbYj5370vz3HDHNmh5EqhKJ5bqs')})
  }
  fetchData(href:string) {
    return this.httpClient.get(href);;
  }
}

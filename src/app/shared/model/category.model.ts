export class Category {
    name:string;
    description:string;
    _links:{
        self:{
            href:string
        },
        category: {
            href:string
        },
        courses: {
            href:string
        }
    };
    public constructor()  {}
}
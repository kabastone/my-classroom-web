export class Course {
            title: string
            description: string
            intro: string
            roadMap:string 
            purpose: string
            skills: string
            img: string
            price: string
            isfree: string
            courseStatus: string
            completed: string
            _links: {
              self: {
                href: string
              }
              course: {
                href: string
              }
              sections: {
                href: string
              }
              author: {
                href: string
              }
            }
          
        constructor(){}
      }

export class Lesson {
        
        _links:{
            self:{
            href:string
        }
    }
    lesson:{
        href:string
    }
    constructor(public title:string,
        public videoUrl:File,
        public duration:number,
        public codeSourceUrl:string) {}
}